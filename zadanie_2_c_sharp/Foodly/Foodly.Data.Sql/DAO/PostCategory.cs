namespace Foodly.Data.Sql.DAO
{
    public class PostCategory
    {
        public int PostCategoryId { get; set; }
        public int PostId { get; set; }
        public int CategoryId { get; set; }
        
        public virtual Category Category { get; set; }
        public virtual Post Post { get; set; }

    }
}