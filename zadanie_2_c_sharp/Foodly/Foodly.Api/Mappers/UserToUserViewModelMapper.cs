using Foodly.Api.ViewModels;

namespace Foodly.Api.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel
            {
                UserId = user.Id,
                Email = user.Email,
                Gender = user.Gender,
                UserName = user.UserName,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.CreationDate,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser,
                IconHref = user.IconHref,
                ThumbnailHref = user.ThumbnailHref,
                PostsCount = user.PostsCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
                AccountDescription = user.AccountDescription,
                AccountPrivateHref = user.AccountPrivateHref,
                UserInfoEditionDate = user.UserInfoEditionDate
            };
            return userViewModel;
        }

    }
}