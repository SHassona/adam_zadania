using System.Threading.Tasks;

namespace Foodly.IData.User
{
    public interface IUserRepository
    {
        Task<int> AddUser(Foodly.Domain.User.User user);
        Task<Foodly.Domain.User.User> GetUser(int userId);
        Task<Foodly.Domain.User.User> GetUser(string userName);
        Task EditUser(Domain.User.User user);
    }
}