using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation;
using Foodly.Common.Enums;

namespace Foodly.Api.BindingModels
{
    public class EditUser
    { 
//        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

//        [Required]
//        [EmailAddress]
//        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
//        [Required]
        [Display(Name = "EditionDate")]
        public DateTime EditionDate { get; set; }
        
//        [Required]
        [Display(Name = "BirthDate")]
        public DateTime BirthDate { get; set; }

        [Display(Name = "Gender")]
//        [Required]
        public Gender Gender { get; set; }
    }
    
    public class EditUserValidator : AbstractValidator<EditUser> {
            public EditUserValidator() {
                    RuleFor(x => x.UserName).NotNull();
                    RuleFor(x => x.BirthDate).NotNull();
                    RuleFor(x => x.Email).EmailAddress();
                    RuleFor(x => x.Gender).NotNull();
            }
    }

}