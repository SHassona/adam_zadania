using System.Threading.Tasks;
using Foodly.IServices.Requests;

namespace Foodly.IServices.User
{
    public interface IUserService
    {
        Task<Foodly.Domain.User.User> GetUserByUserId(int userId);
        Task<Foodly.Domain.User.User> GetUserByUserName(string userName);
        Task<Foodly.Domain.User.User> CreateUser(CreateUser createUser);
        Task EditUser(EditUser createUser, int userId);
    }
}