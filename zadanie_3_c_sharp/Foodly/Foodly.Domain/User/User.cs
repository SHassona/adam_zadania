using System;
using Foodly.Common.Enums;
using Foodly.Domain.DomainExceptions;

namespace Foodly.Domain.User
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; private set; }
        public string Email { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime EditionDate { get; private set; }
        public DateTime UserInfoEditionDate { get; private set; }
        public Gender Gender { get; private set; }
        public DateTime BirthDate { get; private set; }
        public bool IsBannedUser { get; private set; }
        public bool IsActiveUser { get; private set; }
        public int PostsCount { get; private set; }
        public int FollowersCount { get; private set; }
        public int FollowingCount { get; private set; }
        public string AccountDescription { get; private set; }
        public string AccountPrivateHref { get; private set; }
        public string IconHref { get; private set; }
        public string ThumbnailHref { get; private set; }

        public User(int id, string userName, string email, DateTime creationDate, DateTime editionDate, DateTime userInfoEditionDate, Gender gender, DateTime birthDate, bool isBannedUser, bool isActiveUser, int postsCount, int followersCount, int followingCount, string accountDescription, string accountPrivateHref, string iconHref, string thumbnailHref)
        {
            Id = id;
            UserName = userName;
            Email = email;
            CreationDate = creationDate;
            EditionDate = editionDate;
            UserInfoEditionDate = userInfoEditionDate;
            Gender = gender;
            BirthDate = birthDate;
            IsBannedUser = isBannedUser;
            IsActiveUser = isActiveUser;
            PostsCount = postsCount;
            FollowersCount = followersCount;
            FollowingCount = followingCount;
            AccountDescription = accountDescription;
            AccountPrivateHref = accountPrivateHref;
            IconHref = iconHref;
            ThumbnailHref = thumbnailHref;
        }
        public User(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
                throw new InvalidBirthDateException(birthDate);
            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            CreationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            IsBannedUser = false;
            IsActiveUser = true;
            PostsCount = 0;
            FollowersCount = 0;
            FollowingCount = 0;
        }
        
        public void EditUser(string userName, string email, Gender gender, DateTime birthDate)
        {
            if (birthDate >= DateTime.UtcNow)
                throw new InvalidBirthDateException(birthDate);
            UserName = userName;
            Email = email;
            Gender = gender;
            BirthDate = birthDate;
            EditionDate = DateTime.UtcNow;
        }

    }
}