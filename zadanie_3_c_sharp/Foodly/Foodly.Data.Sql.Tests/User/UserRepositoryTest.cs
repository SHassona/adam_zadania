using System;
using System.Threading.Tasks;
using Foodly.Common.Enums;
using Foodly.Data.Sql.User;
using Foodly.IData.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace Foodly.Data.Sql.Tests.User
{
    public class UserRepositoryTest
    {
        public IConfiguration Configuration { get; }
        private  FoodlyDbContext _context;
        private  IUserRepository _userRepository;

        public UserRepositoryTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FoodlyDbContext>();
            optionsBuilder.UseMySQL(
                "server=localhost;userid=root;pwd=rootpass;port=3307;database=foodly_db;");
            _context = new FoodlyDbContext(optionsBuilder.Options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _userRepository = new UserRepository(_context);
        }
        
        [Fact]
        public async Task AddUser_Returns_Correct_Response()
        {
            var user = new Domain.User.User("Name", "Email", Gender.Male, DateTime.UtcNow);
            
            var userId = await _userRepository.AddUser(user);
            
            var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            Assert.NotNull(createdUser);

            _context.User.Remove(createdUser);
            await _context.SaveChangesAsync();
        }

    }
}