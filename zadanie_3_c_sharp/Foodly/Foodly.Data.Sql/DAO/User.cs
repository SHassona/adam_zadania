using System;
using System.Collections.Generic;
using Foodly.Common.Enums;

namespace Foodly.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Preferences = new List<Preference>();
            Posts = new List<Post>();
            RelatingUsers = new List<UserRelation>();
            RelatedUsers = new List<UserRelation>();
            Comments = new List<Comment>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public DateTime UserInfoEditionDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsBannedUser { get; set; }
        public bool IsActiveUser { get; set; }
        public int PostsCount { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public string AccountDescription { get; set; }
        public string AccountPrivateHref { get; set; }
        public string IconHref { get; set; }
        public string ThumbnailHref { get; set; }
        
        public virtual ICollection<Preference> Preferences { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<UserRelation> RelatingUsers { get; set; }
        public virtual ICollection<UserRelation> RelatedUsers { get; set; }

    }
}