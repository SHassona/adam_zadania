using System.Collections.Generic;

namespace Foodly.Data.Sql.DAO
{
    public class Tag
    {
        public Tag()
        {
            PostTags = new List<PostTag>();
        }
        
        public int TagId { get; set; }
        public string TagName { get; set; }
        
        public virtual ICollection<PostTag> PostTags { get; set; }

    }
}