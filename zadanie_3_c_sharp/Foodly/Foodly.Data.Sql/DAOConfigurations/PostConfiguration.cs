using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOConfigurations
{
    public class PostConfiguration: IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.Property(c => c.PostDescription).IsRequired();
            builder.Property(c => c.IsBannedPost).HasColumnType("tinyint(1)");
            builder.Property(c => c.IsActivePost).HasColumnType("tinyint(1)");
            builder.HasOne(x => x.User)
                .WithMany(x => x.Posts)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            builder.ToTable("Post");
        }
    }

}