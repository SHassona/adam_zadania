using System;
using System.ComponentModel.DataAnnotations;
using Foodly.Common.Enums;

namespace Foodly.Api.BindingModels
{
    public class EditUser
    {
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "EditionDate")]
        public DateTime EditionDate { get; set; }
        
        [Required]
        [Display(Name = "BirthDate")]
        public DateTime BirthDate { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }

    }
}