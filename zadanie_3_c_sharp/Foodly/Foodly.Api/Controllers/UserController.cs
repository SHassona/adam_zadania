using System;
using System.Threading.Tasks;
using Foodly.Api.BindingModels;
using Foodly.Api.Validation;
using Foodly.Api.ViewModels;
using Foodly.Data.Sql;
using Foodly.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Foodly.Api.Controllers
{
    [ApiVersion( "1.0" )]
    [Route( "api/v{version:apiVersion}/[controller]" )]
    public class UserController : Controller
    {
        private readonly FoodlyDbContext _context;

        /// <inheritdoc />
        public UserController(FoodlyDbContext context)
        {
            _context = context;
        }
        
        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x=>x.UserId == userId);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    IsActiveUser = user.IsActiveUser,
                    IsBannedUser = user.IsBannedUser,
                    IconHref = user.IconHref,
                    ThumbnailHref = user.ThumbnailHref,
                    PostsCount = user.PostsCount,
                    FollowersCount = user.FollowersCount,
                    FollowingCount = user.FollowingCount,
                    AccountDescription = user.AccountDescription,
                    AccountPrivateHref = user.AccountPrivateHref,
                    UserInfoEditionDate = user.UserInfoEditionDate
                });
            }
            return NotFound();
        }
        
        [HttpGet("name/{userName}", Name = "GetUserByUserName")]
        public async Task<IActionResult> GetUserByUserName(string userName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x=>x.UserName == userName);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    UserId = user.UserId,
                    Email = user.Email,
                    Gender = user.Gender,
                    UserName = user.UserName,
                    BirthDate = user.BirthDate,
                    EditionDate = user.EditionDate,
                    RegistrationDate = user.RegistrationDate,
                    IsActiveUser = user.IsActiveUser,
                    IsBannedUser = user.IsBannedUser,
                    IconHref = user.IconHref,
                    ThumbnailHref = user.ThumbnailHref,
                    PostsCount = user.PostsCount,
                    FollowersCount = user.FollowersCount,
                    FollowingCount = user.FollowingCount,
                    AccountDescription = user.AccountDescription,
                    AccountPrivateHref = user.AccountPrivateHref,
                    UserInfoEditionDate = user.UserInfoEditionDate
                });
            }
            return NotFound();
        }
        
//        [Route("create", Name = "CreateUser")]
        [ValidateModel]
//        [Consumes("application/x-www-form-urlencoded")]
//        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User { Email = createUser.Email,
                UserName = createUser.UserName,
                Gender = createUser.Gender,
                BirthDate = createUser.BirthDate,
                RegistrationDate = DateTime.UtcNow,
                EditionDate = DateTime.UtcNow,
                IsActiveUser = true,
                IsBannedUser = false};
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();
            
            return Created(user.UserId.ToString(), new UserViewModel
            {
                UserId = user.UserId,
                Email = user.Email,
                Gender = user.Gender,
                UserName = user.UserName,
                BirthDate = user.BirthDate,
                EditionDate = user.EditionDate,
                RegistrationDate = user.RegistrationDate,
                IsActiveUser = user.IsActiveUser,
                IsBannedUser = user.IsBannedUser,
                IconHref = user.IconHref,
                ThumbnailHref = user.ThumbnailHref,
                PostsCount = user.PostsCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
                AccountDescription = user.AccountDescription,
                AccountPrivateHref = user.AccountPrivateHref,
                UserInfoEditionDate = user.UserInfoEditionDate
            }) ;
        }
        
        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
//        public async Task<IActionResult> EditUser([FromBody] EditUser editUser,[FromQuery] int userId)
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x=>x.UserId == userId);
            user.UserName = editUser.UserName;
            user.Email = editUser.Email;
            user.BirthDate = editUser.BirthDate;
            user.Gender = editUser.Gender;
            await _context.SaveChangesAsync();
            return NoContent();
        }
    }

}